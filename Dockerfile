FROM steve353/centos:7.4.1708
LABEL Version="1.0.0"
LABEL Description="springinvestor1821"
RUN mkdir /app && chmod 777 /app

# Download Sun Java
RUN yum -y update
RUN yum -y install wget

# Install JConnector and Sun Java 8
RUN wget -nv 'https://www.dropbox.com/s/lqqp8zjc1ibmk8e/jdk-8u131-linux-x64.rpm?dl=0' -O /tmp/jdk-8u131-linux-x64.rpm
RUN yum -y install /tmp/jdk-8u131-linux-x64.rpm

COPY Investor-0.0.1-SNAPSHOT-exec.jar /app/Investor-0.0.1-SNAPSHOT-exec.jar
RUN chmod 777 /app/Investor-0.0.1-SNAPSHOT-exec.jar
EXPOSE 8082
ENTRYPOINT ["java","-jar", "/app/Investor-0.0.1-SNAPSHOT-exec.jar"]

