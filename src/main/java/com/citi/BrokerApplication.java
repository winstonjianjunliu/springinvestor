package com.citi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:application.properties")
public class BrokerApplication {
	
	/**
	 * Run the application as configured.
	 */
	public static void main(String[] args) {
		SpringApplication.run(BrokerApplication.class, args);
	}

}
