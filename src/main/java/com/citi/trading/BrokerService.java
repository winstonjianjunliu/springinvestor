package com.citi.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("accounts")
public class BrokerService {
	
	@Autowired
	private ApplicationContext context;
	
	//@Autowired
	private Map<Integer, Investor> myInvestors = new HashMap<>();
	
	@PostConstruct
	public void init() {
	
			
			Investor investor1 = context.getBean(Investor.class);
			Investor investor2 = context.getBean(Investor.class);
			Investor investor3 = context.getBean(Investor.class);
			
			//set ID
			investor1.setID(1);
			investor2.setID(2);
			investor3.setID(3);
			
			//set portfolio
			Map<String,Integer> portfolio1 = new HashMap<String, Integer> ();
			portfolio1.put("MSFT", 10000);
			investor1.setPortfolio(portfolio1);
			Map<String,Integer> portfolio2 = new HashMap<String, Integer> ();
			investor2.setPortfolio(portfolio2);
			Map<String,Integer> portfolio3 = new HashMap<String, Integer> ();
			investor3.setPortfolio(portfolio3);
			
			//set cash
			investor1.setCash(500.0);
			investor2.setCash(600.0);
			investor3.setCash(700.0);
			
			myInvestors.put(investor1.getID(), investor1);
			myInvestors.put(investor2.getID(), investor2);
			myInvestors.put(investor3.getID(), investor3);
			
			//System.out.println(myInvestors.size());
			//System.out.println(myInvestors.values());
		
	}
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Investor> getAll(){
		List<Investor> allInvestors = new ArrayList<>(myInvestors.values());
		return allInvestors;
	}
	@GetMapping(path="/{ID}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Investor> getByID(@PathVariable("ID") int ID) {
		
		System.out.println ("GET ID=" + ID);
		Investor result = myInvestors.get(ID);
        
        return result != null
            ? new ResponseEntity<Investor> (result, HttpStatus.OK)
            : new ResponseEntity<Investor> (result, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(value="/openAccount")
    @ResponseStatus(HttpStatus.CREATED)
	public Investor openAccount(@RequestParam("cash") double cash) {
		Investor newInvestor = context.getBean(Investor.class);
		newInvestor.setCash(cash);
		
		int curID = myInvestors.size();
		newInvestor.setID(curID+1);
		
		Map<String,Integer> portfolio = new HashMap<String, Integer> ();
		newInvestor.setPortfolio(portfolio);
		
		this.myInvestors.put(curID+1, newInvestor);
		System.out.println("Investor" + newInvestor.getID() + "added.");
		return newInvestor;
	}
	
	@DeleteMapping(value="/closeAccount/{ID}")
	public ResponseEntity<Investor> closeAccount(@PathVariable("ID") int ID) {
		Investor result = myInvestors.get(ID);
        
		if (myInvestors.containsKey(ID)){
			myInvestors.remove(ID);
			System.out.println("Investor" + ID + "removed.");
			return new ResponseEntity<Investor> (result, HttpStatus.OK);
		}
		else {
			System.out.println("Remove investor failed: No such investor");
			return new ResponseEntity<Investor> (result, HttpStatus.NOT_FOUND);
		}
	}


	//auto-generate get all investors
	public Map<Integer, Investor> getMyInvestors() {
		return myInvestors;
	}
}
