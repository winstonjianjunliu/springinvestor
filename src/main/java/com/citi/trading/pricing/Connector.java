package com.citi.trading.pricing;

import java.io.InputStream;
import java.util.function.Function;

public interface Connector extends Function<String, InputStream>{
	
}
