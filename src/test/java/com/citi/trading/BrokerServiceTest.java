package com.citi.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;
import com.citi.trading.pricing.Connector;
import com.citi.trading.pricing.Pricing;
import com.citi.trading.pricing.PricingTest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
public class BrokerServiceTest {
	
	@Configuration
	@ComponentScan
	public static class Config {
		
		
		@Bean
		public OrderPlacer market() {
			return new MockMarket();
		}
		
		@Bean
		@Scope("prototype")
		public Investor investor() {
			Map<String,Integer> portfolioNew = new HashMap<String, Integer> ();
			Investor investor = new Investor();
			investor.setID(5);
			investor.setCash(50.0);
			investor.setPortfolio(portfolioNew);
			return investor;
		}
	}
	
	@Autowired
	private BrokerService bs;
	
	@Before
	public void setup() {
		System.out.println(bs.getMyInvestors().values());
	}
	
	@Test
	public void testGetAll(){
		Map<Integer, Investor> result = bs.getMyInvestors();
		assertThat(result.size(), equalTo(3));
	}
	
//	@Test
//	public void testGetByID(){
//		Map<Integer, Investor> result = bs.getMyInvestors();
//		List<Investor> investors = new ArrayList<>(result.values());
//		assertThat(bs.getByID(1).getID(), equalTo(1));
//		assertThat(bs.getByID(1).getCash(), equalTo(500.0));
//		assertThat(bs.getByID(2).getID(), equalTo(2));
//		assertThat(bs.getByID(2).getCash(), equalTo(600.0));
//	}
//	
//	@Test
//	public void testOpenAccount(){
//		Map<Integer, Investor> result = bs.getMyInvestors();
//		List<Investor> investors = new ArrayList<>(result.values());
//		assertThat(bs.getByID(1).getID(), equalTo(investors.get(0).getID()));
//	}
//	
//
//	@Test
//	public void testCloseAccount(){
//		Map<Integer, Investor> result = bs.getMyInvestors();
//		List<Investor> investors = new ArrayList<>(result.values());
//		assertThat(bs.getByID(1).getID(), equalTo(investors.get(0).getID()));
//	}
	
}
