package com.citi.trading.pricing;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;

/**
 * Integration test for the {@link Pricing} component.
 * 
 * @author Will Provost
 */
public class PricingIntegrationTest {

	/**
	 * A mock subscriber for price updates that simply records all 
	 * notifications so that they can be inspected afterwards.
	 */
	public static class MockSubscriber implements Consumer<PriceData> {
		public List<PriceData> notifications = new ArrayList<>();

		public void accept(PriceData data) {
			notifications.add(data);
		}
	}
	
	public static final String SERVICE_URL = "http://localhost:8081/prices";
	private Pricing pricing = new Pricing();
	

	/**
	 * Test the parsing method for one row of pricing data -- 
	 * a/k/a one price point.
	 */
	@Test
	public void testParsePricePoint() {
		final long MSEC = 1559308530620L;//1559322930620L;
		final String CSV = "2019-05-31 13:15:30.620,717.6573,740.3884,717.6573,731.0316,35420";
		PricePoint pricePoint = Pricing.parsePricePoint(CSV);
		
		assertThat(pricePoint, notNullValue());
		assertThat(pricePoint.getTimestamp().getTime(), equalTo(MSEC));
		assertThat(pricePoint.getOpen(), closeTo(717.6573, 0.000001));
		assertThat(pricePoint.getHigh(), closeTo(740.3884, 0.000001));
		assertThat(pricePoint.getLow(), closeTo(717.6573, 0.000001));
		assertThat(pricePoint.getClose(), closeTo(731.0316, 0.000001));
		assertThat(pricePoint.getVolume(), equalTo(35420));
	}

	/**
	 * Test that subscribers get pricing updates.
	 * But, note that repeated calls for more pricing don't result in more
	 * updates -- because the service won't have new data for another 15 seconds,
	 * and the component under test avoids sending further notifications of
	 * the same pricing data. To check on successive notifications, we'd
	 * need to "sleep" the test thread for 15 seconds at a time.
	 * 
	 * Also note that we can't assert much about the data itself -- 
	 * just that it was received and is well-formed.
	 */
	@Test
	public void testSubscribingAndFetching() throws Exception {
		
		MockSubscriber subscriber = new MockSubscriber();
		pricing.subscribe("GOOG", 4, subscriber);
		try {
			pricing.getPriceData();
			pricing.getPriceData();
			pricing.getPriceData();
			assertThat(subscriber.notifications, 
					contains(hasProperty("size", equalTo(4))));
		} finally {
			pricing.unsubscribe("GOOG", subscriber);
		}
	}
	
	/**
	 * Test that multiple subscribers get the data they need.
	 * 
	 * Note that we can't confirm that the component under test was as
	 * efficient as we want it to be with its outbound HTTP requests.
	 */
	@Test
	public void testMultipleSubscribers() throws Exception {
		
		MockSubscriber googSubscriber = new MockSubscriber();
		MockSubscriber aaSubscriber1 = new MockSubscriber();
		MockSubscriber aaSubscriber2 = new MockSubscriber();
		
		pricing.subscribe("GOOG", 8, googSubscriber);
		pricing.subscribe("AA", 4, aaSubscriber1);
		pricing.subscribe("AA", 8, aaSubscriber2);
		
		try {
			pricing.getPriceData();
			assertThat(googSubscriber.notifications, 
					contains(hasProperty("size", equalTo(8))));
			assertThat(aaSubscriber1.notifications, 
					contains(hasProperty("size", equalTo(8))));
			assertThat(aaSubscriber2.notifications, 
					contains(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", googSubscriber);
			pricing.unsubscribe("AA", aaSubscriber1);
			pricing.unsubscribe("AA", aaSubscriber2);
		}
	}
	
	/**
	 * Test that multiple subscribers get the data they need,
	 * and in the right quantities even when their desired quantities
	 * don't match.
	 * 
	 * Note that we can't confirm that the component under test was as
	 * efficient as we want it to be with its outbound HTTP requests.
	 */
	@Test
	public void testMultipleSubscribersWithExpansion() throws Exception {
		
		MockSubscriber googSubscriber = new MockSubscriber();
		MockSubscriber aaSubscriber1 = new MockSubscriber();
		MockSubscriber aaSubscriber2 = new MockSubscriber();
		
		pricing.subscribe("GOOG", 8, googSubscriber);
		pricing.subscribe("AA", 4, aaSubscriber1);
		
		try {
			pricing.getPriceData();
			assertThat(aaSubscriber1.notifications, 
					contains(hasProperty("size", equalTo(4))));

			pricing.subscribe("AA", 8, aaSubscriber2);
			pricing.getPriceData();
			
			assertThat(googSubscriber.notifications, 
					contains(hasProperty("size", equalTo(8))));
			assertThat(aaSubscriber2.notifications, 
					contains(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", googSubscriber);
			pricing.unsubscribe("AA", aaSubscriber1);
			pricing.unsubscribe("AA", aaSubscriber2);
		}
	}
}

