package com.citi.trading.pricing;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Unit test for the {@link Pricing} component. We use Spring's built-in
 * mock REST server infrastructure to provide mock responses to pricing 
 * requests. Check that we can fetch data and that the publish/subscribe 
 * system works. There is also a test case for the parsing of CSV price
 * data into {@link PricePoint} objects.
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=PricingTest.Config.class)
public class PricingTest {

	public static final String SERVICE_URL = "http://will1.conygre.com:8081/prices/";
	
	public static final String GOOG_INITIAL_VALUES = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:39:30.620,728.8054,737.1855,717.3219,733.5331,15146\n" + 
			"2019-05-31 17:39:45.620,732.8752,765.5307,719.5634,719.5634,94386\n" + 
			"2019-05-31 17:40:00.620,711.8943,711.8943,680.5442,687.6597,48461\n" + 
			"2019-05-31 17:40:15.620,677.7685,731.8210,677.7685,706.9579,53453\n" + 
			"2019-05-31 17:40:30.620,711.6933,711.6933,696.4495,699.5129,63200\n" + 
			"2019-05-31 17:40:45.620,708.9621,722.1723,702.1713,714.6259,5730\n" + 
			"2019-05-31 17:41:00.620,722.2385,722.2385,703.4747,714.9447,7001\n" + 
			"2019-05-31 17:41:15.620,710.9959,776.9483,702.7601,776.9483,65108";
	public static final String GOOG_NEXT_VALUE1 = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:41:30.620,763.0818,763.0818,723.7756,743.8457,39459";
	public static final String GOOG_NEXT_VALUE2 = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:41:45.620,735.6992,766.9705,734.8760,751.8792,84559";
	
	public static final String AA_JUST_FOUR_VALUES = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:39:30.620,728.8054,737.1855,717.3219,733.5331,15146\n" + 
			"2019-05-31 17:39:45.620,732.8752,765.5307,719.5634,719.5634,94386\n" + 
			"2019-05-31 17:40:00.620,711.8943,711.8943,680.5442,687.6597,48461\n" + 
			"2019-05-31 17:40:15.620,677.7685,731.8210,677.7685,706.9579,53453";
	public static final String AA_INITIAL_VALUES = 
			"timestamp,open,high,low,close,volume\n" +
			"2019-05-31 17:39:30.620,728.8054,737.1855,717.3219,733.5331,15146\n" + 
			"2019-05-31 17:39:45.620,732.8752,765.5307,719.5634,719.5634,94386\n" + 
			"2019-05-31 17:40:00.620,711.8943,711.8943,680.5442,687.6597,48461\n" + 
			"2019-05-31 17:40:15.620,677.7685,731.8210,677.7685,706.9579,53453\n" + 
			"2019-05-31 17:40:30.620,711.6933,711.6933,696.4495,699.5129,63200\n" + 
			"2019-05-31 17:40:45.620,708.9621,722.1723,702.1713,714.6259,5730\n" + 
			"2019-05-31 17:41:00.620,722.2385,722.2385,703.4747,714.9447,7001\n" + 
			"2019-05-31 17:41:15.620,710.9959,776.9483,702.7601,776.9483,65108";

	
	@Configuration
	@ComponentScan
	public static class Config {
		
		@Bean
		public Connector Connector() {
			Connector mockConnector = Mockito.mock(Connector.class);
			return mockConnector;
		}
		
		@Bean
		public Pricing Pricing() {
			return new Pricing();
		}

	}
	@Autowired
	private Pricing pricing;
	
	@Autowired
	private Connector myConnector;
	
	@Before
	public void prepare() {
		this.pricing.setConnector(this.myConnector);
	}
	
	/**
	 * A mock subscriber for price updates that simply records all 
	 * notifications so that they can be inspected afterwards.
	 */
	public static class MockSubscriber implements Consumer<PriceData> {
		public List<PriceData> notifications = new ArrayList<>();

		public void accept(PriceData data) {
			notifications.add(data);
		}
	}
	
	/**
	 * Helper to prepare mock responses to HTTP requests.
	 */
	private void prepare(String requestURL, String... responses) {
		OngoingStubbing<InputStream> stubbing = 
				when(myConnector.apply(SERVICE_URL + requestURL));
		for (String response : responses) {
			stubbing = stubbing.thenReturn
					(new ByteArrayInputStream(response.getBytes()));
		}
	}
	
	/**
	 * Test the parsing method for one row of pricing data -- 
	 * a/k/a one price point.
	 */
	@Test
	public void testParsePricePoint() {
		//final long MSEC = 1559322930620L;

		final long MSEC = 1559308530620L;
		final String CSV = "2019-05-31 13:15:30.620,717.6573,740.3884,717.6573,731.0316,35420";
		PricePoint pricePoint = Pricing.parsePricePoint(CSV);
		
		
		assertThat(pricePoint, notNullValue());
		assertThat(pricePoint.getTimestamp().getTime(), equalTo(MSEC));
		assertThat(pricePoint.getOpen(), closeTo(717.6573, 0.000001));
		assertThat(pricePoint.getHigh(), closeTo(740.3884, 0.000001));
		assertThat(pricePoint.getLow(), closeTo(717.6573, 0.000001));
		assertThat(pricePoint.getClose(), closeTo(731.0316, 0.000001));
		assertThat(pricePoint.getVolume(), equalTo(35420));
	}
	
	/**
	 * Test that the component correctly processes one price point as
	 * provided by our mock HTTP service.
	 */
	@Test
	public void testGetOnePrice() throws Exception {
		
		//@SuppressWarnings("unchecked")
		//Function<String,InputStream> connector = mock(Function.class);
		prepare("GOOG?periods=1", GOOG_NEXT_VALUE1);
		//Pricing pricing = new Pricing(connector);
	
		MockSubscriber subscriber = new MockSubscriber();
		pricing.subscribe("GOOG", 1, subscriber);
		try {
			pricing.getPriceData();
			assertThat(subscriber.notifications, hasSize(1));
			assertThat(subscriber.notifications.get(0).getData(1).findAny().get(), 
				allOf(
					hasProperty("open", closeTo(763.0818, 0.0001)),
					hasProperty("high", closeTo(763.0818, 0.0001)),
					hasProperty("low", closeTo(723.7756, 0.0001)),
					hasProperty("close", closeTo(743.8457, 0.0001)),
					hasProperty("volume", equalTo(39459))));
		} finally {
			pricing.unsubscribe("GOOG", subscriber);
		}
	}
	
	/**
	 * Test that subscribers get pricing updates.
	 * Note that, with our mock service primed to respond immediately,
	 * repeated calls for more pricing do result in more updates.
	 */
	@Test
	public void testSubscribingAndFetching() throws Exception {
		
//		@SuppressWarnings("unchecked")
//		Function<String,InputStream> connector = mock(Function.class);
		prepare("GOOG?periods=8", GOOG_INITIAL_VALUES);
		prepare("GOOG?periods=1", GOOG_NEXT_VALUE1, GOOG_NEXT_VALUE2);
		//Pricing pricing = new Pricing(connector);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> subscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, subscriber);
		try {
			pricing.getPriceData();
			pricing.getPriceData();
			pricing.getPriceData();
			verify(subscriber, times(3)).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", subscriber);
		}
	}
	
	/**
	 * Test that subscribers get pricing updates.
	 * Note that, with our mock service primed to respond immediately,
	 * repeated calls for more pricing do result in more updates.
	 * In this test, we have the mock service return a final record that is
	 * stale -- older than the previous one -- and expect the component under test
	 * to ignore it.
	 */
	@Test
	public void testSubscribingAndFetchingOutOfOrder() throws Exception {
		
//		@SuppressWarnings("unchecked")
//		Function<String,InputStream> connector = mock(Function.class);
		prepare("GOOG?periods=8", GOOG_INITIAL_VALUES);
		prepare("GOOG?periods=1", GOOG_NEXT_VALUE2, GOOG_NEXT_VALUE1);
		//Pricing pricing = new Pricing(connector);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> subscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, subscriber);
		try {
			pricing.getPriceData();
			pricing.getPriceData();
			pricing.getPriceData();
			verify(subscriber, times(2)).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", subscriber);
		}
	}
	
	/**
	 * Test that multiple subscribers get the data they need.
	 * Now we also get implicit verification of outbound calls,
	 * because Mockito stubbing is pretty strict and extra HTTP requests
	 * by the component would fail.
	 */
	@Test
	public void testMultipleSubscribers() throws Exception {
		
//		@SuppressWarnings("unchecked")
//		Function<String,InputStream> connector = mock(Function.class);
		prepare("AA?periods=8", AA_INITIAL_VALUES);
		prepare("GOOG?periods=8", GOOG_INITIAL_VALUES);
		//Pricing pricing = new Pricing(connector);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> googSubscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber1 = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber2 = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, googSubscriber);
		pricing.subscribe("AA", 4, aaSubscriber1);
		pricing.subscribe("AA", 8, aaSubscriber2);
		
		try {
			pricing.getPriceData();
			verify(googSubscriber).accept(argThat(hasProperty("size", equalTo(8))));
			verify(aaSubscriber1).accept(argThat(hasProperty("size", equalTo(8))));
			verify(aaSubscriber2).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", googSubscriber);
			pricing.unsubscribe("AA", aaSubscriber1);
			pricing.unsubscribe("AA", aaSubscriber2);
		}
	}
	
	/**
	 * Test that multiple subscribers get the data they need,
	 * and in the right quantities even when their desired quantities
	 * don't match.
	 * Now we also get implicit verification of outbound calls,
	 * because Mockito stubbing is pretty strict and extra HTTP requests
	 * by the component would fail.
	 */
	@Test
	public void testMultipleSubscribersWithExpansion() throws Exception {
		
//		@SuppressWarnings("unchecked")
//		Function<String,InputStream> connector = mock(Function.class);
		prepare("AA?periods=4", AA_JUST_FOUR_VALUES);
		prepare("GOOG?periods=8", GOOG_INITIAL_VALUES);
		prepare("AA?periods=8", AA_INITIAL_VALUES);
		prepare("GOOG?periods=1", GOOG_NEXT_VALUE1);
		//Pricing pricing = new Pricing(connector);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> googSubscriber = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber1 = (Consumer<PriceData>) mock(Consumer.class);
		
		@SuppressWarnings("unchecked")
		Consumer<PriceData> aaSubscriber2 = (Consumer<PriceData>) mock(Consumer.class);
		
		pricing.subscribe("GOOG", 8, googSubscriber);
		pricing.subscribe("AA", 4, aaSubscriber1);
		
		try {
			pricing.getPriceData();
			verify(aaSubscriber1).accept(argThat(hasProperty("size", equalTo(4))));

			pricing.subscribe("AA", 8, aaSubscriber2);
			pricing.getPriceData();
			
			verify(googSubscriber, times(2)).accept(argThat(hasProperty("size", equalTo(8))));
			verify(aaSubscriber2).accept(argThat(hasProperty("size", equalTo(8))));
		} finally {
			pricing.unsubscribe("GOOG", googSubscriber);
			pricing.unsubscribe("AA", aaSubscriber1);
			pricing.unsubscribe("AA", aaSubscriber2);
		}
	}
}

